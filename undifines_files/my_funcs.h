#ifndef MY_FUNCS_H 

#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>

void show_arr(int*, int);
void swap(int*, int*);

void sort_selection(int*, int);
void sort_by_insert(int*, int);
void bubble_sort(int*, int);


#endif

