#include "my_funcs.h"



void 
show_arr(int* arr, int n) {
	for (int i = 0; i < n; i++) {
		printf("%d ", arr[i]);
	}
	printf("\n");
}


void 
swap(int* a, int* b) {
	*a = *b - *a;
	*b = *b - *a;
	*a = *b + *a;
}


void 
sort_selection(int* arr, int n) {
	int i, j, min;
	for (i = 0; i < n - 1; i++) {
		min = i;

		for (j = i + 1; j < n; j++) {
			if (arr[j] < arr[min]) {
				min = j; 

			}
		}
		if (min != i) {
			swap(&arr[min], &arr[i]);
		}
	}

}


void 
sort_by_insert(int* arr, int n) {
	int i, j, key;
	for (i = 1; i < n; i++) {
		key = arr[i]; 
		j = i - 1; 

		while (j >= 0 && arr[j] > key) {
			arr[j+1] = arr[j]; // второму первое значение
			j--; 
		}
		
		arr[j+1] = key; // (первому второе значение), если второе условие из цикла while не выполнится то arr[j+1] просто перезапишется
	}

}


void 
bubble_sort(int* arr, int n) {
	int i, j, f;
	for (i = 0; i < n; i++) {
		f = 1;
		for (j = 0; j < n - i; j++) {
			if (arr[j] > arr[j+1]) {
				swap(&arr[j], &arr[j+1]);
				f = 0;
			}
		}
		if (f == 1) {
			break;
		}
	}

}








