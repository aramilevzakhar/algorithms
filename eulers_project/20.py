def fac(n):
    if n==1:
        return 1
    else:
        return fac(n-1)*n
def ret_sum(n):
    summ=0
    while n!=0:
        summ+=n%10
        n//=10
    return summ

print(ret_sum(fac(100)))
#print(sum(map(lambda x: int(x), list(str(fac(100))))))
