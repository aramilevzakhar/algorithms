fi = open('input.txt', 'r')
s = fi.read()
fi.close()

s = '1' + s + '1'
m = 0
summ = 0
for i in range(len(s)):
	if s[i] == '0':
		summ += 1
	else:
		if m < summ:
			m = summ
		summ = 0

fo = open('output.txt', 'w')
fo.write(str(m))
fo.close()
