#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cmath>
using namespace std;

int main()
{
	ifstream fi;
	char c;
	int count = 0;
	fi.open("input.txt");
		while (!fi.eof()) {
			fi >> c;
			count++;
		}
	fi.close();
	count--;
	char arr[count];
	fi.open("input.txt");
	for (int i = 0; i < count; i++) {
		fi >> c;
		arr[i] = c;
	}
	fi.close();
	int N = atoi(arr);
	if (N > 31) return -1;
	N = pow(2, N) - 1;
	int sum = 1;
	if (N % 2 == 0) {
		sum += N / 2 - 2;
	} else {
		sum += int(N/2) - 1;
	}
	for (int i = 4; i < N + 1; i+=2) {
		if (int(log2(i)) != log2(i)) {
			sum++;
		}
	}
	cout << sum << endl;
	ofstream fo("output.txt");
	fo << sum;
	fo.close();
	return 0;
}
